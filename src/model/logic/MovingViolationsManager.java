package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class MovingViolationsManager implements IMovingViolationsManager {


	private DoubleLinkedList<VOMovingViolations> accidentList = new DoubleLinkedList<>();

	public void loadInfractions(String movingViolationsFile){

CSVReader archivo = null;
		try {

			ArrayList <String>accidentes = new ArrayList<String>();
			 archivo = new CSVReader(movingViolationsFile);
			 archivo.readRecord();
            archivo.readHeaders();
			while (archivo.hasMoreData()){
            
                String codigo= archivo.getRawRecord();
               
                String [] valores = codigo.split(",");
                VOMovingViolations nuevo = new VOMovingViolations();
                if (!valores[0].equals("﻿OBJECTID")) {
                nuevo.setObject_id(Integer.parseInt(valores[0]));
			    nuevo.setRow_(valores[1]);
				nuevo.setLocation(valores[2]);
				nuevo.setAddress_id(valores[3]);
			    nuevo.setStreetsegid(valores[4]);
			    nuevo.setXcoord(Double.parseDouble(valores[5]));
			    nuevo.setYcoord(Double.parseDouble(valores[6]));
				nuevo.setTickettype(valores[7]);
			    nuevo.setFineamt(valores[8]);
			    nuevo.setTotalpaid(valores[9]);
			    nuevo.setPenalty1(valores[10]);
				nuevo.setPenalty2(valores[11]);
			    nuevo.setAccidentindicator(valores[12]);
			    nuevo.setTicketissuedate(valores[13]);
			    nuevo.setViolationcode(valores[14]);
			    nuevo.setViolationdesc(valores[15]);
                accidentList.add(nuevo);
        
                }
			 archivo.readRecord();	 
			}
			



		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}

	public DoubleLinkedList<VOMovingViolations> verifyObjectIDIsUnique()
	{
		
		  DoubleLinkedList<VOMovingViolations> rta = new DoubleLinkedList<>();
		  Map<String, Integer> counts = new HashMap<String, Integer>();
			 ListIterator<VOMovingViolations> iter = accidentList.iterator();
			 
			 while (iter.hasNext())
			 
			 {
			    VOMovingViolations current = iter.next();
			    if (counts.containsKey(current.getObject_id()+"")&&current.getObject_id()!=0)
			    {
			    	counts.put(current.getObject_id()+"", counts.get(current.getObject_id()+"")+1);
			    
			          
			    	   rta.add(current);
			    }
			    else {
			    	counts.put(current.getObject_id()+"",1);
			    }
			   
			    	
			   
			 }
			    
			 return rta;
		
	}
	public Queue <VOMovingViolations> infractionsByHour(LocalDateTime startHour, LocalDateTime finishHour)
	
	{
		  Queue <VOMovingViolations> rta = new  Queue<VOMovingViolations>();
			
			 ListIterator<VOMovingViolations> iter = accidentList.iterator();
			 
			 while (iter.hasNext())
			 
			 {
			    VOMovingViolations current = iter.next();
			    String fechaC = current.getTicketissuedate();
			    if (fechaC.endsWith(".000Z")) {
			    	  fechaC = fechaC.substring(0, fechaC.length() - 5);
			    	}
			    LocalDateTime fecha= convertirFecha_Hora_LDT(fechaC);
			    if (current.getTicketissuedate()!=null && startHour.compareTo(fecha)>=-1&&finishHour.compareTo(fecha)<=1)
			    {
			    	rta.enqueue(current);
			    }
			 }
			 return rta;
	}
	
	
	public int compareTo(LocalDateTime a, LocalDateTime b)
	{ int rta = 1;
		if (a.getHour()< b.getHour())
		{
			return -1;
		}
		
		if (a.getHour()>b.getHour())
		{
			return 1;
		}
		
		else if (a.getMinute()<b.getMinute())
		{
			rta = -1;
		}
		else if (a.getMinute()>b.getMinute())
		{
			return 1;
		}
		else if (a.getSecond()<b.getSecond())
		{
			return -1;
		}
		
		else if (a.getSecond()>b.getSecond())
		{
		  return 1;
		}
		else if(a.getHour()==b.getHour()&&a.getMinute()==b.getMinute()&&a.getSecond()==b.getSecond())
		{
			return 0;
		}
		return rta;
		}
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
	}

	public Stack<VOMovingViolations> infractionsByAddress(String address, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		
		  Stack <VOMovingViolations> rta = new  Stack<VOMovingViolations>();
			
			 ListIterator<VOMovingViolations> iter = accidentList.iterator();
			 
			 while (iter.hasNext())
			 
			 {
			    VOMovingViolations current = iter.next();
			    String fechaC = current.getTicketissuedate();
			    if (fechaC.endsWith(".000Z")) {
			    	  fechaC = fechaC.substring(0, fechaC.length() - 5);
			    	}
			    LocalDateTime fecha = convertirFecha_Hora_LDT(fechaC);
			    if (current.getTicketissuedate()!=null &&current.getAddress_id()!=null&&current.getAddress_id().equals(address)&& fechaInicial.compareTo(fecha)>=-1&&fechaFinal.compareTo(fecha)<=1)
			    {  
			    	rta.push(current);
			    }
			    
			 }
		
			 return rta;
	}

		
	public static Stack<VOMovingViolations> sortstack(Stack<VOMovingViolations> input) 
{ 
Stack<VOMovingViolations> tmpStack = new Stack<VOMovingViolations>(); 
while(!input.isEmpty()) 
{ 
// pop out the first element 

VOMovingViolations tmp = input.pop(); 

// while temporary stack is not empty and 
// top of stack is greater than temp 
while(!tmpStack.isEmpty() && tmpStack.peek().getStreetsegid()  
        .compareTo(tmp.getStreetsegid())>=-1) 
{ 
// pop from temporary stack and  
// push it to the input stack 
input.push(tmpStack.pop()); 
} 

// push temp in tempory of stack 
tmpStack.push(tmp); 
} 
return tmpStack; 
}

	public double[] avg(String violationCode3) {
		 ListIterator<VOMovingViolations> iter = accidentList.iterator();
		 double[] rta = new double[ ] {};
		 while (iter.hasNext())
		 
		 {
		    VOMovingViolations current = iter.next();

		    if (current.getViolationcode().equals(violationCode3)) 
		    {
		    
		    }
		
		 }
		return rta;
	} 
	}


